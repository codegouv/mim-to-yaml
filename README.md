# mim-to-yaml

Convert MIMx ([mutualisation inter-ministérielle](http://pcll.ac-dijon.fr/mim/)) Ethercalc files to a Git repository of YAML files.

## Usage

```bash
./mim_to_yaml.py data/
```
