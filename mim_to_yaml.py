#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# mim-to-yaml -- Convert MIMx Ethercalc files to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/codegouv/mim-to-yaml
#
# mim-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# mim-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import codecs
import collections
import csv
import datetime
import logging
import os
import sys
import urllib.request

import yaml


# YAML configuration


class folded_unicode(str):
    pass


class literal_unicode(str):
    pass


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.items()))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)

yaml.add_representer(folded_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
clean_labels = {
    '': 'SECTEUR',
    'Date de mise à jour de la ligne': 'DATE MAJ',
    'DATE DERNIÈRE MAJ LIGNE': 'DATE MAJ',
    'LI-CEN-CE': 'LICENCE',
    'VER-SION': 'VERSION',
    }
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for generated YAML files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    for spreadsheet_name, spreadsheet_url in (
            ('mimdev', 'http://eole.ac-dijon.fr/ethercalc/mimdev2016.csv'),
            ('mimo', 'http://eole.ac-dijon.fr/ethercalc/mimo2016.csv'),
            ('mimprod', 'http://eole.ac-dijon.fr/ethercalc/mimprod2016.csv'),
            ):
        response = urllib.request.urlopen(spreadsheet_url)
        reader = csv.reader(codecs.iterdecode(response, 'utf-8'))
        header = [
            ' '.join(clean_labels.get(cell, cell).split()).strip()
            for cell in next(reader)
            ]
        for row in reader:
            entry = collections.OrderedDict(
                (key, value)
                for key, value in zip(header, row)
                if value
                )
            if not entry:
                continue
            update_date = entry.get('DATE MAJ')
            if update_date is not None:
                if '/' in update_date:
                    day, month, year = update_date.split('/')
                    update_date = datetime.date(int(year), int(month), int(day))
                else:
                    update_date = datetime.datetime(1900, 1, 1) + datetime.timedelta(days = int(update_date) - 2)
                entry['DATE MAJ'] = update_date
            software_name = entry.get('LOGICIEL')
            if not software_name:
                print(spreadsheet_name, entry)
                continue
            software_name = '-'.join(software_name.lower().replace('/', ' ').replace(':', ' ').replace('–', ' ')
                .split()).strip()
            with open(os.path.join(args.yaml_dir, '{}.yaml'.format(software_name.lower())), 'w') as yaml_file:
                yaml.dump(entry, yaml_file, allow_unicode = True, default_flow_style = False, indent = 2, width = 120)

    return 0


if __name__ == "__main__":
    sys.exit(main())
